// VARIABLES
const cartList = document.querySelector('#cart-list');
const totalPrice = document.querySelector('#total-price');
const submitBtn = document.querySelector('#sub-btn');
const errorMessage = document.querySelector('#error-message');
const userFormInputs = document.querySelectorAll('#userForm input');
const recaptchaWidget = document.querySelector('#recaptcha-widget');

const userEmailId = '#email';
const userNameId = '#name';
const userAddressId = '#address';
const userPhoneId = '#phone';

const userEmail = document.querySelector(userEmailId);
const userName = document.querySelector(userNameId);
const userAddress = document.querySelector(userAddressId);
const userPhone = document.querySelector(userPhoneId);

const userInputDataErrors = {
    [userEmailId]: { errMes: 'Please enter a valid email address.' },
    [userNameId]: { errMes:  'Please enter a valid name. Use only latin letters.' },
    [userAddressId]: { errMes: 'Please enter a valid address. Specify the address according to template: <City>, st. <StreetName> <building number>/<flat number>. Use only latin letters.' },
    [userPhoneId]: { errMes: 'Please enter a valid phone number. Specify the phone number according to template: +1 23456789' }
};

let totalPriceValue = 0;


// FUNCTIONS

const reloadRecaptcha = () => {
    grecaptcha.reset(recaptchaWidget);
}

const calcTotalPrice = () => {
    let cartItems = JSON.parse(localStorage.getItem("cartItems"));
    if (cartItems) {
        let sum = 0;
        cartItems.forEach((i) => {
            sum += i.price * i.count;
        });
        totalPriceValue = sum.toFixed(2)
        totalPrice.innerHTML = `Total Price: $${totalPriceValue}`;
    }
}

const updateCart = () => {
    cartList.innerHTML = '';

    let cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];

    let content = '';
    cartItems.forEach(el => {
        content += `
            <li class="cart-item" id="item${el.id}">
                <img src="${el.imageURL}" alt="Item image">
                <div class="item-details">
                    <div class="item-name">${el.itemName}</div>
                    <div class="item-price">$${el.price}</div>
                    <div class="item-actions">
                        <div class="counter">
                            <button class="decrease-button" id="dec" data-item-id="${el.id}">-</button>
                            <input type="text" class="item-count" id="item-count" data-item-id="${el.id}" pattern="^\\d+$" value="${el.count}">
                            <button class="increase-button" id="inc" data-item-id="${el.id}">+</button>
                        </div>
                        <div class="remove-button" id="rem" data-item-id="${el.id}">Remove</div>
                    </div>
                </div>
            </li>
        `;
    });
    cartList.insertAdjacentHTML('beforeend', content);
    calcTotalPrice();
}

const removeItem = (e) => {
    let itemIdToRemove = e.target.getAttribute('data-item-id');
    let cartItems = JSON.parse(localStorage.getItem("cartItems"));
    let updatedCartItems = cartItems.filter((item) => {
        return item.id !== itemIdToRemove;
    });
    localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
    updateCart();
}

const increaseItemCount = (e) => {
    let itemId = e.target.getAttribute('data-item-id');
    let cartItems = JSON.parse(localStorage.getItem("cartItems"));

    for (const item of cartItems) {
        if (item.id === itemId) {
            item.count++;
            break;
        }
    }
    localStorage.setItem("cartItems", JSON.stringify(cartItems));
    updateCart();
}

const decreaseItemCount = (e) => {
    let itemId = e.target.getAttribute('data-item-id');
    let cartItems = JSON.parse(localStorage.getItem("cartItems"));

    for (const item of cartItems) {
        if (item.id === itemId) {
            if (item.count == 1) {
                break;
            } else {
                item.count--;
                break;
            }
        }
    }
    localStorage.setItem("cartItems", JSON.stringify(cartItems));
    updateCart();
}

const loadUserData = () => {
    let userData = JSON.parse(localStorage.getItem("userData"));
    if (userData) {
        userEmail.value = userData.email;
        userName.value = userData.name;
        userAddress.value = userData.address;
        userPhone.value = userData.phone;
    }
}

const saveUserData = () => {
    let userData = {
        email: userEmail.value,
        name: userName.value,
        address: userAddress.value,
        phone: userPhone.value
    };
    localStorage.setItem("userData", JSON.stringify(userData));
}

const checkRecaptcha = async () => {
    const captcha  = document.querySelector('#g-recaptcha-response').value;

    fetch('/verify-recaptcha', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ captcha }),
    })
    .then(response => response.json())
    .then((data) => {
        console.log('data: ')
        console.log(data)
        // Handle the response from the server
        if (data.success) {
            // reCAPTCHA verification successful
            return true;
        } else {
            // reCAPTCHA verification failed
            return false;
        }
    })
    .catch((error) => {
        console.error('Error verifying reCAPTCHA:', error);
        reloadRecaptcha();
    });
}

const checkInputValidity = (elem, errorMes) => {
    if (!elem.checkValidity()) {
        errorMessage.style.display = 'block';
        errorMessage.textContent = errorMes;
        reloadRecaptcha();
    } else {
        errorMessage.style.display = 'none';
    }
}

const submitFunc = async () => {
    let shopId = JSON.parse(localStorage.getItem("shopId"));
    let cartItems = JSON.parse(localStorage.getItem("cartItems"));

    for (const el of userFormInputs) {
        if (!el.checkValidity()) {
            errorMessage.style.display = 'block';
            errorMessage.textContent = 'Invalid user data!';
            return;
        }
    }

    saveUserData();

    errorMessage.style.display = 'block';
    if (!checkRecaptcha()) {
        errorMessage.textContent = 'Error verifying reCAPTCHA.';
        return;
    }
    errorMessage.style.display = 'none';

    if (shopId && cartItems.length != 0) {
        let orderData = {
            email: userEmail.value,
            name: userName.value,
            address: userAddress.value,
            phone: userPhone.value,
            order: {
                shopId: shopId,
                totalPrice: totalPriceValue,
                items: cartItems
            }
        };
        fetch('/order', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(orderData),
        })
        .then(response => response.json())
        .then(savedOrderData => {
            localStorage.setItem("cartItems", JSON.stringify([]));
            updateCart();
            location.reload();
        })
        .catch(error => {
            // console.error(error);
        });
    }
    reloadRecaptcha();
}


// EVENTS
window.addEventListener('load', () => {
    updateCart();
    loadUserData();
});

userFormInputs.forEach((el) => {
    el.addEventListener('change', (e) => {
        checkInputValidity(el, userInputDataErrors[`#${e.target.id}`].errMes);
    });
});

cartList.addEventListener('click', (e) => {
    if (e.target.id === 'inc') {
        increaseItemCount(e);
    } else if (e.target.id === 'dec') {
        decreaseItemCount(e);
    } else if (e.target.id === 'rem') {
        removeItem(e);
    }
});

cartList.addEventListener('change', (e) => {
    if (e.target.id === 'item-count') {
        if (e.target.checkValidity()) {
            let itemId = e.target.getAttribute('data-item-id');
            let cartItems = JSON.parse(localStorage.getItem("cartItems"));
            let enteredCount = parseInt(e.target.value);

            for (const item of cartItems) {
                if (item.id === itemId) {
                    if (enteredCount == 0) {
                        item.count = 1;
                        break;
                    } else {
                        item.count = enteredCount;
                        break;
                    }
                }
            }
            localStorage.setItem("cartItems", JSON.stringify(cartItems));
            updateCart();
        }
    }
});

submitBtn.addEventListener('click', submitFunc);
