// VARIABLES
const shopList = document.querySelector('#shop-list');
const itemColumn = document.querySelector('#item-column');
const shopListLiElements = document.querySelectorAll('#shop-list li');


// FUNCTIONS
const addToCart = (e) => {
    const itemId = e.target.getAttribute('data-item-id');
    let cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    let itemExist = false;

    if (cartItems.length != 0) {
        for (const item of cartItems) {
            if (item.id == itemId) {
                item.count += 1;
                itemExist = true;
                break;
            } else {
                itemExist = false;
            }
        }
    }

    if (!itemExist) {
        let newItem = {
            id: itemId,
            count: 1,
            imageURL: document.querySelector('#item-' + itemId + ' img').src,
            itemName: document.querySelector('#item-' + itemId + ' .item-name').textContent,
            price: document.querySelector('#item-' + itemId + ' .item-price').textContent.substring(1)
        };
        cartItems.push(newItem);
    }
    localStorage.setItem("cartItems", JSON.stringify(cartItems));
}

const showItems = (items) => {
    itemColumn.innerHTML = '';

    // Generate HTML for each item and append it to the list
    items.forEach(item => {
        const content = `
            <div class="item" id="item-${item.itemId}">
                <div class="item-image">
                    <img src="${item.imageURL}" alt="Item Image">
                </div>
                <div class="item-details">
                    <span class="item-name">${item.itemName}</span><br>
                    <span class="item-price">$${item.price.toFixed(2)}</span>
                </div>
                <button class="add-to-cart-button" id="item-button" data-item-id="${item.itemId}">Add to Cart</button>
            </div>
        `;

        itemColumn.insertAdjacentHTML('beforeend', content);
    });

    const addToCartBtns = document.querySelectorAll('#item-button');
    addToCartBtns.forEach((i) => {
        i.addEventListener('click', addToCart)
    });
}

const getShopItems = (shopId) => {
    if (shopId) {
        fetch(`/shops/${shopId}/items`)
        .then(response => response.json())
        .then((shop) => {
            showItems(shop.items);
            localStorage.setItem("shopName", JSON.stringify(shop.name));
            localStorage.setItem("shopAddress", JSON.stringify(shop.address));
        })
        .catch(error => {
            // console.error(error);
        });
    }
}

const openSelectedShop = () => {
    let shopId = JSON.parse(localStorage.getItem("shopId")) || '';
    if (shopId != '') {
        for (const li of shopListLiElements) {
            if (li.getAttribute('data-shop-id') == shopId) {
                li.classList.add('active');
                getShopItems(shopId);
                break;
            }
        }
    }
}


// EVENTS
window.addEventListener('load', () => {
    openSelectedShop();
});

shopList.addEventListener('click', (e) => {
    if (e.target.tagName != 'LI') {
        return;
    }
    let swapFlag = false;
    let cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
    let localShopId = JSON.parse(localStorage.getItem("shopId"));
    let elShopId = e.target.getAttribute('data-shop-id');
    if (cartItems.length != 0) {
        if (elShopId != localShopId) {
            let confirmSwap = window.confirm('Are you sure you want to swap shops?\nYour cart will be cleared.');
            if (confirmSwap) {
                swapFlag = true;
            }
        }
    } else {
        swapFlag = true;
    }

    if (swapFlag) {
        getShopItems(elShopId);
        localStorage.setItem("shopId", JSON.stringify(elShopId));
        localStorage.setItem("cartItems", JSON.stringify([]));

        shopListLiElements.forEach((li) => {
            li.classList.remove('active');
        });
        e.target.classList.add('active');
    }
});
