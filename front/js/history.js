// VARIABLES
const historyBtn = document.querySelector('#history-btn');
const orderCardsElement = document.getElementById("order-cards");
const userEmailHistory = document.querySelector('#email');
const userPhoneHistory = document.querySelector('#phone');
const historyErrorMessage = document.querySelector('#error-message');


// FUNCTIONS
const generateOrderHistoryHTML = (userData) => {
    userData = {
        orders: userData[0].orders
    };

    let orderCardsHTML = '';

    userData.orders.forEach(order => {
        let itemsHTML = '';

        order.items.forEach(item => {
            const itemHTML = `
                <div class="item-container">
                    <img src="${item.imageURL}" alt="${item.itemName}" class="item-image">
                    <div class="item-details">
                    <div class="item-name">${item.itemName}</div>
                    <div class="item-price">$${item.price.toFixed(2)}</div>
                    </div>
                </div>
            `;
            itemsHTML += itemHTML;
        });

        const orderCardHTML = `
            <div class="order-card">
            ${itemsHTML}
            <div class="total-price">Total: $${order.totalPrice.toFixed(2)}</div>
            </div>
        `;
        orderCardsHTML += orderCardHTML;
    });
    return orderCardsHTML;
};

const loadUserHistory = async () => {

    historyErrorMessage.style.display = 'block';
    if (!userEmailHistory.checkValidity()) {
        historyErrorMessage.textContent = 'Please enter a valid email address.';
        return;
    }

    if (!userPhoneHistory.checkValidity()) {
        historyErrorMessage.textContent = 'Please enter a valid phone number.';
        return;
    }
    historyErrorMessage.style.display = 'none';

    let orderData = {
        userEmailHistory: userEmailHistory.value,
        userPhoneHistory: userPhoneHistory.value
    }

    fetch("/history", {
        method: "POST",
        headers: {
        "Content-Type": "application/json"
        },
        body: JSON.stringify(orderData)
    })
    .then(response => {
        if (response.ok) {
            return response.json();
        } else {
            throw new Error("Error retrieving user order history");
        }
    })
    .then(userOrderHistory => {
        const orderHistoryHTML = generateOrderHistoryHTML(userOrderHistory);
        orderCardsElement.innerHTML = orderHistoryHTML;
    })
    .catch(error => {
        console.error("Error retrieving user order history:", error);
        orderCardsElement.innerHTML = '';
    });
}

const loadUserDataForHistory = () => {
    let userData = JSON.parse(localStorage.getItem("userData"));
    if (userData) {
        userEmailHistory.value = userData.email;
        userPhoneHistory.value = userData.phone;
    }
}


// EVENTS
window.addEventListener('load', () => {
    loadUserDataForHistory();
});

historyBtn.addEventListener('click', () => {
    loadUserHistory();
});


