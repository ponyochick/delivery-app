// VARIABLES
const mapWidget = document.querySelector('#map');
const userMapAddress = document.querySelector('#address');
const deliveryTime = document.querySelector('#delivery-time');

let map;
let markers = [];


// FUNCTIONS

async function initMap() {
    const position = { lat: 52.23, lng: 21.00 };

    const { Map } = await google.maps.importLibrary("maps");

    map = new Map(mapWidget, {
        zoom: 12,
        center: position,
    });
}

// Show address on the map
const showAddress = (address, markerName) => {
    const geocoder = new google.maps.Geocoder();

    geocoder.geocode({ address: address }, (results, status) => {
        if (status === 'OK') {
            let location = results[0].geometry.location;
            map.setCenter(location);
            const marker = new google.maps.Marker({
                map: map,
                position: location,
                title: markerName
            });
            markers.push(marker);
        } else {
            console.log('Geocode was not successful for the following reason: ' + status);
        }
    });
}

// Show route from shop to user's address
// const showRoute = (userAddress, shopAddress) => {
//     let geocoder = new google.maps.Geocoder();

//     geocoder.geocode({ address: shopAddress }, (shopResults, shopStatus) => {
//         if (shopStatus === 'OK') {
//             let shopLocation = shopResults[0].geometry.location;
//             geocoder.geocode({ address: userAddress }, (userResults, userStatus) => {
//                 if (userStatus === 'OK') {
//                     let userLocation = userResults[0].geometry.location;

//                     let directionsService = new google.maps.DirectionsService();
//                     let directionsDisplay = new google.maps.DirectionsRenderer({ map: map });

//                     let request = {
//                         origin: shopLocation,
//                         destination: userLocation,
//                         travelMode: google.maps.TravelMode.DRIVING
//                     };

//                     directionsService.route(request, (response, status) => {
//                         if (status === 'OK') {
//                             directionsDisplay.setDirections(response);
//                         } else {
//                             console.log('Directions request failed due to ' + status);
//                         }
//                     });
//                 } else {
//                     console.log('Geocode was not successful for the following reason: ' + userStatus);
//                 }
//             });
//         } else {
//             console.log('Geocode was not successful for the following reason: ' + shopStatus);
//         }
//     });
// }

// Show route from shop to user's address
const showRoute = (userAddress, shopAddress) => {
    let geocoder = new google.maps.Geocoder();

    geocoder.geocode({ address: shopAddress }, (shopResults, shopStatus) => {
        if (shopStatus === 'OK') {
            let shopLocation = shopResults[0].geometry.location;
            geocoder.geocode({ address: userAddress }, (userResults, userStatus) => {
                if (userStatus === 'OK') {
                    let userLocation = userResults[0].geometry.location;

                    let directionsService = new google.maps.DirectionsService();
                    let directionsDisplay = new google.maps.DirectionsRenderer({ map: map });

                    let request = {
                        origin: shopLocation,
                        destination: userLocation,
                        travelMode: google.maps.TravelMode.DRIVING
                    };

                    directionsService.route(request, (response, status) => {
                        if (status === 'OK') {
                            directionsDisplay.setDirections(response);

                            // Access the duration information
                            let route = response.routes[0];
                            let duration = route.legs[0].duration.text;

                            // Display the approximate time
                            // console.log('Approximate time: ' + duration);
                            deliveryTime.innerHTML = `Approximate delivery time: ${duration}`;
                        } else {
                            console.log('Directions request failed due to ' + status);
                        }
                    });
                } else {
                    console.log('Geocode was not successful for the following reason: ' + userStatus);
                }
            });
        } else {
            console.log('Geocode was not successful for the following reason: ' + shopStatus);
        }
    });
};

// Remove all markers from the map
const removeMarkers = (ind = '') => {
    if (ind === '') {
        // Remove all markers from the map
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(null); // Set the marker's map property to null
        }
        markers = []; // Clear the markers array
    } else {
        // Remove a specific marker by index
        if (ind >= 0 && ind < markers.length) {
            markers[ind].setMap(null); // Set the marker's map property to null
            markers.splice(ind, 1); // Remove the marker from the markers array
        }
    }
}

const getShopData = () => {
    let shopName = JSON.parse(localStorage.getItem("shopName")) || 'Shop';
    let shopAddress = JSON.parse(localStorage.getItem("shopAddress")) || '';
    return { shopName, shopAddress };
}

const userMapAddressHandler = () => {
    let shop = getShopData();
    if (shop.shopAddress != '') {
        initMap();
        showRoute(userMapAddress.value, shop.shopAddress);
    }
}


// EVENTS
window.addEventListener('load', () => {
    initMap();

    if (userMapAddress.checkValidity()) {
        userMapAddressHandler();
    } else {
        let shop = getShopData();
        if (shop.shopAddress != '') {
            showAddress(shop.shopAddress, shop.shopName);
        }
    }
});

userMapAddress.addEventListener('change', () => {
    if (userMapAddress.checkValidity()) {
        userMapAddressHandler();
    }
});
