# Delivery App

This repository contains the source code for the Delivery App project.

## Requirements

* MongoDB version 6.0.x or higher

## Getting started
### Installing MongoDB:
1. Visit the official [MongoDB website](https://www.mongodb.com/).
2. Download the appropriate installation package for your operating system (Windows, macOS, or Linux).
3. Run the installer and follow the installation wizard to configure MongoDB.
4. Make sure to add the MongoDB bin directory to your system's PATH environment variable.

### Configuration MongoDB:

1. Start the MongoDB server by running the following command in your terminal:
```
mongod
```
By default, it will run on **`mongodb://localhost:27017`**.

2. Create a new database with the name **`delivery`**.
3. Create two new collections with the following names:
    * **`shops-items`**
    * **`users`**

4. You can use the provided JSON files located in the **`delivery-app/mongodb`** directory:
    * Use **`shops-items.json`** to import data into the **`shops-items`** collection.
    * Use **`users.json`** to import data into the **`users`** collection.


### Running App:

1. Clone the repository:
```
git clone git@gitlab.com:ponyochick/delivery-app.git
```

2. Install project dependencies:
```
cd delivery-app/back
npm install
```

3. Run:
```
node server.js
```

4. Open:
- [http://localhost:3000/](http://localhost:3000/)
