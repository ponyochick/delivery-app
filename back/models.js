import mongoose from 'mongoose';


// SHOP
const shopSchema = new mongoose.Schema({
    id: Number,
    name: String,
    address: String,
    items: [{
        itemId: Number,
        imageURL: String,
        itemName: String,
        price: Number
    }]
});
const ShopModel = mongoose.model('shops-items', shopSchema);

// USER
const userSchema = new mongoose.Schema({
    email: String,
    name: String,
    address: String,
    phone: String,
    orders: [{
        shopId: String,
        totalPrice: Number,
        items: [{
            itemId: Number,
            imageURL: String,
            itemName: String,
            price: Number
        }]
    }]
});
const UserModel = mongoose.model('users', userSchema);

export { ShopModel, UserModel };