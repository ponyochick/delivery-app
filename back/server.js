import { fileURLToPath } from 'url';
import { dirname, join } from 'path';
import express from 'express';
import mongoose from 'mongoose';

import { ShopModel, UserModel } from './models.js';


// VARIABLES
const nameRegex = /^[A-Za-z\s\-']+$/;
const addressRegex = /^[A-Za-z\s]+,\sst\.\s[A-Za-z\s]+\s\d+[A-Za-z]?(\/\d+)?$/;
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const phoneRegex = /^\+\d{1,3}\s\d+$/;

// DB connection
await mongoose.connect('mongodb://127.0.0.1:27017/delivery', {
   useNewUrlParser: true,
   useUnifiedTopology: true,
})
.then(() => console.log('Connected to MongoDB'))
.catch((error) => console.error('MongoDB connection error:', error));


// PREPARING

const app = express();
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const __front = join(__dirname, '..', 'front');
const __back = join(__dirname, '..', 'back');

// Serve static files from the 'front' folder
app.use(express.static(__front));
const viewsPath = join(__front, 'views');

// Set EJS as the template engine
app.set('view engine', 'ejs');
app.set('views', viewsPath);

// Set parser
app.use(express.json());

// ROUTER

app.get('/', (req, res) => {
   res.redirect('/shops');
});

app.get('/shops', async (req, res, next) => {
    try {
        const results = await ShopModel.find();
        res.render("pages/shops", { shops: results });
    } catch (error) {
        console.log('Failed: ' + error);
    }
});

app.get('/shops/:shopId/items', async (req, res) => {
    try {
        const shopId = req.params.shopId;
        const shop = await ShopModel.findOne({ id: shopId });
        res.json(shop);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
});

app.get('/cart', (req, res) => {
    res.render('pages/cart');
});

app.get('/history', (req, res) => {
    res.render('pages/history');
});

app.post('/order', (req, res) => {
    const orderData = req.body;

    // Validate name field
    if (!nameRegex.test(orderData.name)) {
        return res.status(400).json({ error: 'Invalid name.' });
    }

    // Validate address field
    if (!addressRegex.test(orderData.address)) {
        return res.status(400).json({ error: 'Invalid address.' });
    }

    // Validate email field
    if (!emailRegex.test(orderData.email)) {
        return res.status(400).json({ error: 'Invalid email.' });
    }

    // Validate phone field
    if (!phoneRegex.test(orderData.phone)) {
        return res.status(400).json({ error: 'Invalid phone number.' });
    }

    UserModel.findOne({ $and: [{ email: orderData.email }, { phone: orderData.phone }] })
    .then(existingUser => {
        if (existingUser) {
            existingUser.orders.push(orderData.order);

            existingUser.save()
            .then(savedUser => {
                console.log(savedUser);
                res.json(savedUser);
            })
            .catch(error => {
                res.status(500).json({ error: 'An error occurred while saving the order.' });
            });
        } else {
            const newUser = new UserModel(orderData);
            newUser.orders.push(orderData.order)

            newUser.save()
            .then(savedUser => {
                console.log('new user', savedUser);
                res.json(savedUser);
            })
            .catch(error => {
                res.status(500).json({ error: 'An error occurred while saving the user.' });
            });
        }
    })
    .catch(error => {
        res.status(500).json({ error: 'An error occurred while checking for existing objects.' });
    });
});

app.post('/history', (req, res) => {
    const orderData = req.body;

    // Validate email field
    if (!emailRegex.test(orderData.userEmailHistory)) {
        return res.status(400).json({ error: 'Invalid email.' });
    }

    // Validate phone field
    if (!phoneRegex.test(orderData.userPhoneHistory)) {
        return res.status(400).json({ error: 'Invalid phone number.' });
    }

    // Search the user order history in the database
    UserModel.find({ $and: [{ email: orderData.userEmailHistory }, { phone: orderData.userPhoneHistory }] })
    .then(userOrderHistory => {
        res.send(userOrderHistory);
    })
    .catch(error => {
        console.error('Error retrieving user order history:', error);
        res.status(500).send('Error retrieving user order history');
    });
});

app.post('/verify-recaptcha', async (req, res) => {
    const { captcha } = req.body;
    const secretKey = '6LcMiUYmAAAAAJYrLgg_u08BjSeFgjk_IHbJFHm4';
    const userIp = req.ip;

    try {
        const response = await fetch('https://google.com/recaptcha/api/siteverify', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: new URLSearchParams({
            secret: secretKey,
            response: captcha,
            remoteip: userIp,
        }).toString(),
    });

    const data = await response.json();
    console.log(data)

    if (data.success) {
        // reCAPTCHA response is valid
        res.json(data);
    } else {
        // reCAPTCHA response is invalid
        res.json(data);
    }
    } catch (error) {
        console.error('Error verifying reCAPTCHA:', error);
        res.status(500).json({ success: false });
    }
});


const port = 3000;
app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});